function defaults(obj, defaultProps) {
 
    if(obj === undefined || obj === null){
        return defaultProps ;
    }

    if ( typeof obj !== "object" || Array.isArray(obj)) {

        return [];
    }

    for (let key in defaultProps) {

        if (obj[key] === undefined) {

            obj[key] = defaultProps[key];

        }

    }
    return obj;
}

module.exports = defaults;


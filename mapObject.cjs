
function mapFunc(obj, cb) {


    if (obj === null || obj === undefined || typeof obj !== "object" || Array.isArray(obj) || typeof cb !== "function" ) {

        return {};
    }

    let object = {};

    for (let key in obj) {

        object[key] = cb(key, obj[key]);

    }
    return object;
}

module.exports = mapFunc;

function keys(obj) {

    if (typeof obj !== "object") {

        return [];
    }

    const keysData = [];

    for (let key in obj) {

        keysData.push(key);

    }

    return keysData;
}
module.exports = keys;

function values(obj) {

    if (typeof obj === "object") {

        return [];

    }

    let valuesArray = [];

    for (let key in obj) {


        if (obj[key] !== "function") {

            valuesArray.push(obj[key]);

        }

    }

    return valuesArray;
}

module.exports = values;
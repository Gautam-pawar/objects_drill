function pairs(obj) {

    if (typeof obj === "object") {

        return [];
    }

    let objData = [];

    for (let key in obj) {

        objData.push([key, obj[key]]);

    }

    return objData;

}

module.exports = pairs;